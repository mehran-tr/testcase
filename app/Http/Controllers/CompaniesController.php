<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class CompaniesController extends Controller
{
    public function addCompany(Request $req)
    {
        $companies = new Companies();
        if ($req->post()) {
            $companies->company_name = $req->cName;
            $companies->location = $req->location;
            $companies->description = $req->cDescription;
            $companies->owner = $req->owner;
            $companies->financial_performance = $req->financial_performance;
            $companies->employees_number = $req->employees_number;
            $companies->foundation_date = $req->foundationDate;
            if ($companies->save()) {
                redirect('dashboard');
            }
        }

        return view('addCompany');
    }

    public function jsonFile()
    {
        $jsonString = file_get_contents(base_path('product.json'));

        $data = json_decode($jsonString, true);

        return view('json', ['jsonFile' => $data]);


    }

    public function uploadJson(Request $req)
    {

        $this->validate($req, [
            'file' => 'required|file|mimes:json'
        ]);
        return $req->file('file')->store('files');
    }


    public function csvFile()
    {
        return view('csv');
    }

    public function uploadCsv(Request $req)
    {
        $upload = $req->file('file');
        $filePath = $upload->getRealPath();
        $file = fopen($filePath,'r');
        $header = fgetcsv($file);

        $escapedHeader = [];
        foreach ($header as $key => $value){
            $lheader = strtolower($value);

            $escapedItem = preg_replace('/[^a-z]/','',$lheader);

            array_push($escapedHeader,$escapedItem);
        }


        //looping through othe columns
        while($columns=fgetcsv($file)) {
            if ($columns[0] == "") {
                continue;
            }

            $data = array_combine($escapedHeader, $columns);


            // Table update
            $company_name = $data['companyname'];
            $location = $data['location'];
            $description = $data['description'];
            $owner  = $data['owner'];
            $foundation_date = $data['foundationdate'];
            $employees_number = $data['employeesnumber'];
            $financial_performance = $data['financialperformance'];

            $company = Companies::firstOrNew(['company_name' => $company_name, 'location' => $location]);
            $company->description = $description;
            $company->owner = $owner;
            $company->foundation_date = $foundation_date;
            $company->employees_number = $employees_number;
            $company->financial_performance = $financial_performance;
            $company->save();
        }
    }
}


