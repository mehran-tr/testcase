<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Str;
use Faker\Factory as Faker;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,50) as $value){
            DB::table('companies')->insert([
                'company_name' => $faker->name,
                'location' => $faker->country,
                'description' => $faker->realText,
                'owner' => $faker->name,
                'financial_performance' => $faker->randomNumber(),
                'foundation_date' => $faker->dateTime,
                'employees_number' => $faker->numberBetween(100,50000),

            ]);
        }
    }
}
